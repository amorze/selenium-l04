package com.mycompany.capabilities;

public enum Browsers {
    FIREFOX("firefox"), CHROME("chrome");

    private String browser;

    Browsers(String browser) {
        this.browser = browser;
    }

    @Override
    public String toString() {
        return browser;
    }
}
