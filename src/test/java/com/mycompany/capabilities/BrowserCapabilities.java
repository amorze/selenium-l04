package com.mycompany.capabilities;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;

public class BrowserCapabilities {

    public static Capabilities getCapabilities(Browsers browser) {

        switch (browser) {
            case FIREFOX: {
                FirefoxOptions firefoxCapabilities = new FirefoxOptions()
                        .addPreference("app.update.auto", false)
                        .addPreference("app.update.enabled", false)
                        .addPreference("network.proxy.no_proxies_on", "localhost");
                firefoxCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                firefoxCapabilities.setHeadless(true);
                firefoxCapabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
                firefoxCapabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
                return firefoxCapabilities;
            }

            case CHROME: {
                ChromeOptions chromeCapabilities = new ChromeOptions()
                        .addArguments("--start-maximized")
                        .addArguments("--incognito")
                        .addArguments("--headless")
                        .addArguments("--ignore-certificate-errors");
                chromeCapabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
                chromeCapabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
                return chromeCapabilities;
            }

            default:
                throw new IllegalArgumentException("Invalid browser: " + browser);
        }
    }
}
