package com.mycompany.tests;

import com.mycompany.capabilities.BrowserCapabilities;
import com.mycompany.capabilities.Browsers;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver driver;

    @BeforeMethod
    public void initData() {
//        Capabilities capabilities = BrowserCapabilities.getCapabilities(Browsers.FIREFOX);
//        driver = new FirefoxDriver((FirefoxOptions) capabilities);

        Capabilities capabilities = BrowserCapabilities.getCapabilities(Browsers.CHROME);
        driver = new ChromeDriver((ChromeOptions) capabilities);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://cw07529-wordpress.tw1.ru/my-account/");
    }

    @AfterMethod
    public void stop() {
        if (driver != null) {
            driver.quit();
        }
    }
}
