package com.mycompany.tests;

import com.mycompany.data.Credentials;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LoginTest extends BaseTest {

    @Test(description = "Login Page - valid credentials")
    public void login() {

        //login field
        WebElement userNameInput = driver.findElement(By.cssSelector("#username"));
        userNameInput.sendKeys(Credentials.userName);

        //password field
        WebElement passwordInput = driver.findElement(By.cssSelector("#password"));
        passwordInput.sendKeys(Credentials.userPassword);

        //click login button
        WebElement loginButton = driver.findElement(By.cssSelector(".woocommerce-form-login__submit"));
        loginButton.click();

        assertEquals(driver.findElements(By.cssSelector("ul.woocommerce-error > li")).size(), 0);
    }
}
